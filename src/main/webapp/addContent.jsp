<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
    <script type="text/javascript" src="/resources/scripts/jquery-3.1.1.js"></script>
    <script type="text/javascript">
        $(document).ready(function ($) {
            $('.add-song-button').click(function () {
                $('.form').hide();
                $('.song-form').show();
            });

            $('.add-audiobook-button').click(function () {
                $('.form').hide();
                $('.audiobook-form').show();
            });

            $('.add-podcast-button').click(function () {
                $('.form').hide();
                $('.podcast-form').show();
            });
        });
    </script>
</head>
<body>

    <h3 class="page-heading">Add content below:</h3>

    <div class="content-type-selection">
        <div class="content-type-button content-type-song">
            <button class="button add-song-button">Add song</button>
        </div>
        <div class="content-type-button content-type-audiobook">
            <button class="button add-audiobook-button">Add audiobook</button>
        </div>
        <div class="content-type-button content-type-podcast">
            <button class="button add-podcast-button">Add podcast</button>
        </div>
    </div>
    <div class="add-content-form">

        <div class="song-form form" hidden="hidden">
            <form action="addContent" method="post">

                <label for="fileName1">File Name</label>
                <input type="text" name="fileName" id="fileName1" required="required" />
                <label for="fileContentName1">File Content</label>
                <input type="text" name="fileContentName" id="fileContentName1" required="required" />
                <label for="fileDuration1">Duration</label>
                <input type="number" name="fileDuration" id="fileDuration1" required="required" />
                <label for="fileSize1">Size</label>
                <input type="number" name="fileSize" id="fileSize1" required="required" />
                <label for="price1">Price(optional)</label>
                <input type="number" name="price" id="price1" />
                <label for="description1">Description(optional)</label>
                <input type="text" name="description" id="description1" />
                <label for="performer">Performer</label>
                <input type="text" name="performer" id="performer" />
                <label for="album">Album</label>
                <input type="text" name="album" id="album" />
                <label for="year">Year</label>
                <input type="number" name="year" id="year" />

                <div class="actions">
                    <button class="button cancel-button" value="Cancel" >Cancel</button>
                    <input type="submit" value="Submit" class="button save-content-button" />
                </div>
            </form>
        </div>

        <div class="audiobook-form form" hidden="hidden">
            <form action="addContent" method="post">

                <label for="fileName2">File Name</label>
                <input type="text" name="fileName" id="fileName2" required="required" />
                <label for="fileContentName2">File Content</label>
                <input type="text" name="fileContentName" id="fileContentName2" required="required" />
                <label for="fileDuration2">Duration</label>
                <input type="number" name="fileDuration" id="fileDuration2" required="required" />
                <label for="fileSize2">Size</label>
                <input type="number" name="fileSize" id="fileSize2" required="required" />
                <label for="price2">Price(optional)</label>
                <input type="number" name="price" id="price2" />
                <label for="description2">Description(optional)</label>
                <input type="text" name="description" id="description2" />
                <label for="author">Author</label>
                <input type="text" name="author" id="author" required="required" />
                <label for="genre">Genre</label>
                <input type="text" name="genre" id="genre" required="required" />
                <label for="language">Language</label>
                <input type="text" name="language" id="language" required="required" />
                <label for="year2">Year</label>
                <input type="number" name="year" id="year2" required="required" />

                <div class="actions">
                    <button class="button cancel-button" value="Cancel" >Cancel</button>
                    <input type="submit" value="Submit" class="button save-content-button" />
                </div>
            </form>
        </div>

        <div class="podcast-form form" hidden="hidden">
            <form action="addContent" method="post">

                <label for="fileName3">File Name</label>
                <input type="text" name="fileName" id="fileName3" required="required" />
                <label for="fileContentName3">File Content</label>
                <input type="text" name="fileContentName" id="fileContentName3" required="required" />
                <label for="fileDuration3">Duration</label>
                <input type="number" name="fileDuration" id="fileDuration3" required="required" />
                <label for="fileSize3">Size</label>
                <input type="number" name="fileSize" id="fileSize3" required="required" />
                <label for="price3">Price(optional)</label>
                <input type="number" name="price" id="price3" />
                <label for="description3">Description(optional)</label>
                <input type="text" name="description" id="description3" />
                <label for="topic">Topic</label>
                <input type="text" name="topic" id="topic" required="required" />
                <label for="episode">Episode</label>
                <input type="number" name="episode" id="episode" required="required" />
                <label for="date">Date</label>
                <input type="date" name="date" id="date" required="required" />

                <div class="actions">
                    <button class="button cancel-button" value="Cancel" >Cancel</button>
                    <input type="submit" value="Submit" class="button save-content-button" />
                </div>
            </form>
        </div>
    </div>

</body>
</html>
