<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Pmx-client</title>
</head>
<body>
    <c:set var="songClass" value="SongType" />
    <c:set var="audioBookClass" value="AudiobookType" />
    <c:set var="podcastClass" value="PodcastType" />
    <h2 class="page-heading">PMX-CLIENT</h2>
     <div class="content-section">
        <table class="content-table">
            <thead>
                <tr>
                    <th>TYPE</th>
                    <th>NAME</th>
                    <th>PRICE</th>
                    <th>DESCRIPTION</th>
                </tr>
            </thead>
            <tbody>
            <c:forEach var="item" items="${content.songOrPodcastOrAudiobook}">
                <tr>
                    <c:if test="${item['class'].simpleName eq 'SongType'}">
                        <td>SONG</td>
                    </c:if>
                    <c:if test="${item['class'].simpleName eq audioBookClass}">
                        <td>AUDIOBOOK</td>
                    </c:if>
                    <c:if test="${item['class'].simpleName eq podcastClass}">
                        <td>PODCAST</td>
                    </c:if>
                    <td>${item.fileContentName}</td>
                    <td>${item.price}</td>
                    <td>${item.description}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
     </div>
    <div class="actions">
        <button class="button add-content-button"><a href="/addContent">Add Files</a></button>
        <button class="button delete-content-button"><a href="/deleteContent">Delete Files</a></button>
    </div>
</body>
</html>

