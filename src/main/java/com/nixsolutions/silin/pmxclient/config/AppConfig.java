package com.nixsolutions.silin.pmxclient.config;

import com.nixsolutions.silin.pmxclient.services.PmxSoapContentClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

/**
 * Created by diman on 05.03.17.
 */
@Configuration
@ComponentScan
public class AppConfig {

    @Bean
    public PmxSoapContentClient pmxSoapContentClient(Jaxb2Marshaller marshaller) {
        PmxSoapContentClient client = new PmxSoapContentClient();
        client.setDefaultUri("http://localhost:8585/soap/contentPort/content.wsdl");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("com.nixsolutions.silin.pmxclient.soap");
        return marshaller;
    }


}
