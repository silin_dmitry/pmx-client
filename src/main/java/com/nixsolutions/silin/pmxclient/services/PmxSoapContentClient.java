package com.nixsolutions.silin.pmxclient.services;

import com.nixsolutions.silin.pmxclient.services.PmxSoapContentClient;
import com.nixsolutions.silin.pmxclient.soap.*;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by diman on 05.03.17.
 */
public class PmxSoapContentClient extends WebServiceGatewaySupport {

    private static final String PMX_URI = "http://localhost:8585/soap/contentPort/content.wsdl";

    public GetAllContentResponse getAllContent() {
        GetAllContentRequest request = new GetAllContentRequest();
        GetAllContentResponse response = (GetAllContentResponse) getWebServiceTemplate()
                .marshalSendAndReceive(PMX_URI, request,
                        new SoapActionCallback("localhost:8585/soap/getAllContentResponse"));
        return response;
    }

    public GetContentResponse getContent(List<BigInteger> ids) {
        GetContentRequest request = new GetContentRequest();
        request.setFileId(ids);
        GetContentResponse response = (GetContentResponse) getWebServiceTemplate()
                .marshalSendAndReceive(PMX_URI, request,
                        new SoapActionCallback("localhost:8585/soap/getContentResponse"));
        return response;
    }

    public SaveContentResponse saveContent(Content content) {
        SaveContentRequest request = new SaveContentRequest();
        request.setContent(content);
        SaveContentResponse response = (SaveContentResponse) getWebServiceTemplate()
                .marshalSendAndReceive(PMX_URI, request,
                        new SoapActionCallback("localhost:8585/soap/saveContentResponse"));
        return response;
    }

    public void deleteContent(List<BigInteger> ids) {
        DeleteContentRequest request = new DeleteContentRequest();
        request.setId(ids);
        getWebServiceTemplate().marshalSendAndReceive(PMX_URI, request);
    }

}
