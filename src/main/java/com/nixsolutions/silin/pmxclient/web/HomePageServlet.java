package com.nixsolutions.silin.pmxclient.web;

import com.nixsolutions.silin.pmxclient.services.PmxSoapContentClient;
import com.nixsolutions.silin.pmxclient.soap.Content;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by diman on 04.03.17.
 */
@WebServlet("/home")
public class HomePageServlet extends HttpServlet{

    private PmxSoapContentClient pmxSoapContentClient;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        WebApplicationContext applicationContext = WebApplicationContextUtils
                .getWebApplicationContext(getServletContext());
        pmxSoapContentClient = applicationContext.getBean(PmxSoapContentClient.class);
        Content content = pmxSoapContentClient.getAllContent().getContent();
        req.setAttribute("content", content);
        req.getRequestDispatcher("home.jsp").forward(req, resp);
    }
}
