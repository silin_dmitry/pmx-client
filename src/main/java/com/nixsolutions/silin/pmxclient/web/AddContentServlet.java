package com.nixsolutions.silin.pmxclient.web;

import com.nixsolutions.silin.pmxclient.soap.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by diman on 04.03.17.
 */
@WebServlet("/addContent")
public class AddContentServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("addContent.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.sendRedirect("/home");
    }

    private Content prepareContent(HttpServletRequest req) {
        Content content = new Content();
        AudioFileType file = prepareFile(req);
        content.getSongOrPodcastOrAudiobook().add(file);
        return content;
    }

    private AudioFileType prepareFile(HttpServletRequest req) {
        AudioFileType file;
        String fileType = req.getParameter("fileType");
        if (fileType.equals("song")) {
            file = new SongType();
            populateCommonFields(file, req);
        }
        else if (fileType.equals("audiobook")) {
            file = new AudiobookType();
            populateCommonFields(file, req);
        }
        else {
            file = new PodcastType();
            populateCommonFields(file, req);
        }
        return file;
    }

    private void populateCommonFields(AudioFileType file, HttpServletRequest req) {
        file.setFileId(0l);
        file.setFileName(req.getParameter("fileName"));
        file.setFileContentName(req.getParameter("fileContentName"));
        file.setFileDuration(new BigInteger(req.getParameter("fileDuration")));
        file.setFileSize(new BigDecimal(req.getParameter("fileSize")));
        file.setPrice(new BigDecimal(req.getParameter("price")));
        file.setDescription(req.getParameter("description"));
    }
}
